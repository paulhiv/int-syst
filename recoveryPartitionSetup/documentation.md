## mise en place d'un systeme de backup sur la machine centos


se script backup le dossier home ainsi que les fichiers qui contienes le server web et sa configuration

se backup se trouve dans un dossier nomé archive

## mise en place

- ajouter un dique de 1 gb sur la vm centos via vmware

- monter le dique dur externe sur la machine:

```
vgcreate vgdata /dev/sdb;
mke2fs -t ext3 /dev/dm-2 
mount -t ext3 /dev/dm-2 /mnt
```

## installation 


ajouter les droits d'execution du script `` chmod +x ./backupScript``

ajouter le script dans la crontab avec `` crontab -e``

```
        0 0 * * 0,3,6 $HOME/backupScript
```

