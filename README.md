# présentation

ceci est un mise en place d'un lab pour scanner les vulnérabilitées dans un réseau

# infrastructure

mise en place avec vmware
```
 |---------------|       |------------------------|
 | server apache |       | server apache          |
 | centos        |-------| ubuntu metasploitable  |
 | 10.10.100.5   |   |   |       10.10.100.4      |
 |---------------|   |   |------------------------|
                     |
                     |
                     |
            |------------------|
            | server openvas   |
            |   10.10.100.3    |
            |------------------|
```




- Server apache Centos redondé Raid 1 avec un backup de sa base de donné
    
    ```
    ram: 1024mb
    stockage: 2 disques dur 20gb
    interface: host-only 10.10.100.5
    ```

- Server apache Ubuntu vulnérable metasploitable 2

    ```
    ram: 1024mb
    stockage: 1 disque dur 8gb
    interface: host-only 10.10.100.4
    ```

- Scanner de vulnérabilité openVAS en web-interface

    ```
    ram: 4096
    stockage: 1 disque dur 9gb
    interface: nat 192.168.43.130
               host-only 10.10.100.3
    ```

# Mise en place

## instalation

### Centos

- configurer la carte reseau sur vmware en NAT

- durant l'installation mettre les deux disques en RAID
et mettre toutes les partions en RAID avec ``/ = md0; /boot = md1; /swap = md2``

    ```
    df -Hp
    Filesystem      Size  Used Avail Use% Mounted on
    devtmpfs        476M     0  476M   0% /dev
    tmpfs           487M     0  487M   0% /dev/shm 
    tmpfs           487M  7.8M  479M   2% /run
    tmpfs           487M     0  487M   0% /sys/fs/cgroup
    /dev/md126       18G  1.5G   17G   9% /
    /dev/md125     1019M  161M  859M  16% /boot
    tmpfs            98M     0   98M   0% /run/user/0
    ```

- configuer l'interface reseau en remplisant le ficher ``/etc/sysconfig/sysconfig/network-scripts/ifcfg-ens33`` avec :
    ```
    TYPE="Ethernet"
    BOOTPROTO="dhcp"
    NAME="ens33"
    UUID="2740dd66-1baa-4383-bf41-2bf719669cde"
    DEVICE="ens33"
    ONBOOT=yes
    ```
    ensuite redémarer l'interface avec ``ifdown ens33 && ifup ens33``

- installer mariadb et apache avec ``yum install httpd mariadb -y``

- mettre en place le server apache avec 
    ```
    systemctl enable httpd
    systemctl start httpd
    ```
- mettre en place la base de donnée avec
    ```
    systemctl enable mariadb
    systemctl start mariadb
    mysql_secure_installation
    y
    y
    n
    y
    ```

- eteindre la vm et changer la carte réseau en hostonly vmNet2 en 10.10.100.0/29

#### mise en place du backup de base de donnée

suivre et installer les scripts sur [sql backup](https://gitlab.com/paulhiv/int-syst/tree/master/sql%20backup%20script)

### Metasploitable

- configuer la carte réseau en host-only vmNet2

- aucune configuration  suplémentaire

### openVAS

- ajouter une carte réseau en host-only vmNet2

- parcourir le l'installation guidée

- aller dans l'option "network" et selectioner dhcp pour les deux interfaces